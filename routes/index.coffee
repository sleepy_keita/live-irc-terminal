# GET home page.
fs = require("fs")
escape = require("escape-html")
exports.index = (req, res) ->
  db = req.db

  messages = []

  db.each "SELECT * FROM messages ORDER BY timestamp DESC", (err, row) ->
    messages.push
      from: row.nick
      text: row.text
      gravatar: row.gravatar
  , () ->
    res.render "index",
      messages: messages

    return

  return
