jQuery(function($) {
  var socket = io.connect(document.location.href), $chatbox;

  $chatbox = $('#chatbox');

  socket.on('new_message', function (data) {
    var $chatline, inner;

    inner = Templates['message']({
      message: data
    });

    $chatline = $(inner).prependTo($chatbox);
    $chatline.addClass('highlight');

    window.setTimeout(function() {
      $chatline.slideDown(200, function() {
        window.setTimeout(function() {
          $chatline.removeClass('highlight');
        }, 500);
      });
    }, 0);

  });

});
