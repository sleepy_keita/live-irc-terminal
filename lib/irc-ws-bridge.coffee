config  = require '../config'

path    = require 'path'
fs      = require 'fs'
moment  = require 'moment'
irc     = require 'irc'
escape  = require 'escape-html'
io      = require 'socket.io'
crypto  = require 'crypto'

module.exports = (server, db) ->
  chatlog = fs.createWriteStream path.join(__dirname, '../chatlog.txt'),
    flags: 'a+'
    encoding: 'utf8'

  ws = io.listen server

  gravatarStorage = {}

  gravatarForNick = (nick, callback) ->
    if gravatarStorage[nick]
      callback "http://www.gravatar.com/avatar/#{ gravatarStorage[nick] }.jpg"
    else
      bot.whois nick, (info) ->
        email = info.realname.toLowerCase()

        console.log email

        md5hash = crypto.createHash 'md5'
        md5hash.update email

        hexHash = md5hash.digest 'hex'
        gravatarStorage[nick] = hexHash

        callback "http://www.gravatar.com/avatar/#{ hexHash }.jpg"

  bot = new irc.Client config.irc.server, config.irc.nick,
    channels: [config.irc.channel]
    userName: config.irc.nick
    realName: config.irc.nick

  bot.on 'error', (message) ->
    console.log "IRC Error: ", message

  bot.on 'message', (from, to, text, message) ->
    nowMoment = moment()

    gravatarForNick from, (gravatar) ->

      ws.sockets.emit 'new_message',
        from: from
        gravatar: gravatar
        text: escape text

      db.run "INSERT INTO messages (nick, timestamp, \"text\", gravatar) VALUES ($nick, $timestamp, $text, $gravatar)",
        $nick: from
        $timestamp: Math.floor(nowMoment.toDate().getTime() / 1000)
        $text: text
        $gravatar: gravatar

    chatlog.write "(#{ nowMoment.format 'YYYY-MM-DD HH:mm:ss' }) #{ from }: #{ text }\n", 'utf8'

  bot.join config.irc.channel
