express      = require("express")
http         = require("http")
path         = require("path")
favicon      = require("static-favicon")
logger       = require("morgan")
bodyParser   = require("body-parser")
rack         = require("asset-rack")
sqlite3      = require("sqlite3")
routes       = require("./routes")
app          = express()

# view engine setup
app.set "views", path.join(__dirname, "views")
app.set "view engine", "jade"
app.use favicon()
app.use logger("dev")
app.use bodyParser.json()
app.use bodyParser.urlencoded()
app.use express.static(path.join(__dirname, "public"))

# Asset Rack
assets = new rack.Rack [
  new rack.JadeAsset
    url: '/javascripts/templates.js'
    dirname: './views/partials'
]

app.use assets

db = new sqlite3.Database path.join(__dirname, "chatlog.sqlite3")
db.serialize () ->
  db.run "CREATE TABLE IF NOT EXISTS messages (id INTEGER PRIMARY KEY, nick VARCHAR, timestamp INTEGER, \"text\" TEXT, gravatar VARCHAR)"

app.db = db
app.use (req, res, next) ->
  req.db = db
  next()

app.use app.router
app.get "/", routes.index

#/ catch 404 and forwarding to error handler
app.use (req, res, next) ->
  err = new Error("Not Found")
  err.status = 404
  next err
  return

#/ error handlers

# development error handler
# will print stacktrace
if app.get("env") is "development"
  app.use (err, req, res, next) ->
    res.render "error",
      message: err.message
      error: err

    return


# production error handler
# no stacktraces leaked to user
app.use (err, req, res, next) ->
  res.render "error",
    message: err.message
    error: {}

  return

process.on 'exit', () ->
  db.close()

module.exports = app
