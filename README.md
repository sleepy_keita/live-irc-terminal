# Live-IRC Terminal

A quick Node app to stream an IRC channel into a browser window, intended for presentation.

# Configuration

0. `$ npm install`
1. Copy `config.example.js` to `config.js`
2. Fill in appropriate values.

# Running

```
$ npm start
```

or

```
$ ./bin/www
```

The terminal should be accessible at [http://localhost:3000/](http://localhost:3000/) (or at the port number defined in the `PORT` environment variable, if it exists)